import { Injectable } from "@angular/core";
import { AuthService } from './auth';
import firebase from 'firebase';

@Injectable()
export class DataService {
  private database: any;
  private currentUser: any;

  constructor(private authService: AuthService) {
    this.database = firebase.database();
    this.currentUser = this.authService.getCurrentUser();
  }

  updatePersonData(name: string, lastname: string, department: string, company: string, position: string) {
    return this.database.ref('users/' + (this.currentUser.uid)).set({
      uid: this.currentUser.uid,
      user: this.currentUser.email,
      name: name,
      lastname: lastname,
      department: department,
      company: company,
      position: position
    });
  }

  getPersonData() {
    let ref = this.database.ref('users');
    return ref.orderByChild('uid').equalTo(this.currentUser.uid);
  }

  getBusSeatByPerson(uid_busId_date: string) {
    let ref = this.database.ref('bookings');
    return ref.orderByChild('uid_busId_date').equalTo(uid_busId_date);
  }

  bookBusSeat(date: string, busId: number, busName: string, seatId: number) {
    let $this = this;
    let promise = new Promise((resolve, reject) => {
      $this.getPersonData().once('value', function(result) {
        if (result.val()) {
          let userData = Object.keys(result.val()).map(key => result.val()[key])[0];
          $this.getBusSeatByPerson(userData.uid + '_' + busId + '_' + date).once('value', function(data) {
            if (!data.val()) {
              $this.database.ref('bookings/' + (new Date()).getTime()).set({
                uid: userData.uid,
                user: userData.user,
                name: userData.name + ' ' + userData.lastname,
                date: date,
                busId: busId,
                busName: busName,
                seat: seatId,
                busId_date: busId + '_' + date,
                uid_busId_date: userData.uid + '_' + busId + '_' + date
              }).then(() => {
                resolve();
              }).catch(error => {
                reject();
              });
            } else {
              reject({message: 'Ya ha reservado un asiento para el bus y fecha seleccionados'});
            }
          });
        } else {
          reject({message: 'Por favor actualice sus datos primero!'});
        }
      });
    });
    return promise;
  }

  getPassengerList(busId: number, date: string) {
    let ref = this.database.ref('bookings');
    return ref.orderByChild('busId_date').equalTo(busId + '_' + date);
  }

  getBookingsToApprove() {
    let ref = this.database.ref('bookings');
    return ref.orderByChild('date');
  }
}
