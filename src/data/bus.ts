export default [
  { id: 1, name: 'Arequipa - Bus #1' },
  { id: 2, name: 'Arequipa - Bus #2' },
  { id: 3, name: 'Arequipa - Bus #3' },
  { id: 4, name: 'Arequipa - Bus #4' },
  { id: 5, name: 'Arequipa - Bus #5' },
  { id: 6, name: 'Arequipa - Bus #6' },
  { id: 7, name: 'Juliaca - Bus #1' },
  { id: 8, name: 'Espinar - Bus #1' }
]