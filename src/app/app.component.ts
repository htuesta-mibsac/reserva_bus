import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';

import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { PersonPage } from '../pages/person/person';
import { ApproveBookingsPage } from '../pages/approve-bookings/approve-bookings';
import { ApproveUsersPage } from '../pages/approve-users/approve-users';

import { AuthService } from '../services/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  signinPage = SigninPage;
  signupPage = SignupPage;
  personPage = PersonPage;
  approveBookingsPage = ApproveBookingsPage;
  approveUsersPage = ApproveUsersPage;
  isAuthenticated = false;
  @ViewChild('nav') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private authService: AuthService, private menuCtrl: MenuController) {
    firebase.initializeApp({
      apiKey: "AIzaSyAb9b7CIqf80fvZWJl6QWVwA2RBmPUG5GA",
      authDomain: "reserva-bus.firebaseapp.com",
      databaseURL: "https://reserva-bus.firebaseio.com",
      projectId: "reserva-bus",
      storageBucket: "reserva-bus.appspot.com",
      messagingSenderId: "1026926620581"
    });
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.isAuthenticated = true;
        this.rootPage = HomePage;
      } else {
        this.isAuthenticated = false;
        this.rootPage = SigninPage;
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCtrl.close();
  }

  onLogout() {
    this.authService.logout();
    this.menuCtrl.close();
    this.nav.setRoot(SigninPage);
  }
}
