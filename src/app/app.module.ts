import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';
import { PersonPage } from '../pages/person/person';
import { BusPage } from '../pages/bus/bus';
import { PassengerPage } from '../pages/passenger/passenger';
import { ApproveBookingsPage } from '../pages/approve-bookings/approve-bookings';
import { ApproveUsersPage } from '../pages/approve-users/approve-users';
import { AuthService } from '../services/auth';
import { DataService } from '../services/data';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SignupPage,
    SigninPage,
    BusPage,
    PassengerPage,
    PersonPage,
    ApproveBookingsPage,
    ApproveUsersPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SignupPage,
    SigninPage,
    BusPage,
    PassengerPage,
    PersonPage,
    ApproveBookingsPage,
    ApproveUsersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    DataService
  ]
})
export class AppModule {}
