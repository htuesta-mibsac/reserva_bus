import { Component } from '@angular/core';
import { NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DataService } from '../../services/data';
import busSeats from '../../data/seats';

@Component({
  selector: 'page-bus',
  templateUrl: 'bus.html',
})
export class BusPage {
  private seats: Array<any>;
  private totalSeats: number;
  private bus: any;
  private tripDate: string;
  private logonUser: any;
  private bookedSeats: Array<any> = [];

  constructor(private navParams: NavParams, private alertController: AlertController, 
    private loadingCtrl: LoadingController, private dataService: DataService) {
    this.seats = busSeats;
    this.totalSeats = this.getTotalSeats();
    this.bus = this.navParams.get('bus');
    this.tripDate = this.navParams.get('date');
    this.logonUser = this.navParams.get('user');

    this.refreshBusLayout();
  }

  getTotalSeats() {
    let total = 0;
    for (let i=0; i < this.seats.length; i++) {
      for (let j=0; j < this.seats[i].length; j++) {
        total++;
      }
    }
    return total;
  }

  refreshBusLayout(){
    const loading = this.loadingCtrl.create({
      content: 'Obteniendo datos...'
    });
    loading.present();
    let $this = this;
    this.dataService.getPassengerList(this.bus.id, this.tripDate).once('value', function(result) {
      if (result.val()) {
        $this.bookedSeats = Object.keys(result.val()).map(key => result.val()[key].seat).sort((a, b) => a.seat - b.seat);
      }
      loading.dismiss();
    });
  }

  isSeatAvailable(seatId: number) {
    return !(this.bookedSeats.indexOf(seatId) > -1);
  }

  selectSeat(seatId: number) {
    let $this = this;
    this.dataService.bookBusSeat(this.tripDate, this.bus.id, this.bus.name, seatId).then(result => {
      const alert = $this.alertController.create({
        title: 'Éxito!',
        message: 'Reserva guardada para el día: ' + $this.tripDate + '\n Bus: ' + $this.bus.name + 
          '\n Asiento seleccionado: ' + seatId,
        buttons: [{
          text: 'OK',
          handler: () => {
            this.refreshBusLayout();
          }
        }]
      });
      alert.present();
    }).catch(error => {
      const alert = $this.alertController.create({
        title: 'Error',
        message: error.message,
        buttons: ['OK']
      });
      alert.present();
    });
  }
}
