import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from 'ionic-angular';
import { DataService } from '../../services/data';

@Component({
  selector: 'page-person',
  templateUrl: 'person.html',
})
export class PersonPage {
  private name: string;
  private lastname: string;
  private department: string;
  private company: string;
  private position: string;

  constructor(private loadingCtrl: LoadingController, private alertCtrl: AlertController,
    private dataService: DataService) {
    const loading = this.loadingCtrl.create({
      content: 'Obteniendo datos...'
    });
    loading.present();
    let $this = this;
    this.dataService.getPersonData().once('value', function(result) {
      if (result.val()) {
        let userData = Object.keys(result.val()).map(key => result.val()[key])[0];
        $this.name = userData.name;
        $this.lastname = userData.lastname;
      }
      loading.dismiss();
    });
  }

  onUpdateData(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Registrando...'
    });
    loading.present();
    this.dataService.updatePersonData(form.value.name, form.value.lastname,
      form.value.department, form.value.company, form.value.position).then(result => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Los datos se actualizaron correctamente!',
        buttons: ['OK']
      });
      alert.present();
    }).catch(error => {
      loading.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error al actualizar los datos!',
        message: error.message,
        buttons: ['OK']
      });
      alert.present();
    });
  }
}
