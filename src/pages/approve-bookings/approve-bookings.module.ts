import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApproveBookingsPage } from './approve-bookings';

@NgModule({
  declarations: [
    ApproveBookingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ApproveBookingsPage),
  ],
})
export class ApproveBookingsPageModule {}
