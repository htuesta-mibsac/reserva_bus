import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from 'ionic-angular';
import { AuthService } from '../../services/auth';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {
  constructor(private loadingCtrl: LoadingController, private authService: AuthService, 
    private alertCtrl: AlertController) {}

  onSignin(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Ingresando...'
    });
    loading.present();
    this.authService.signin(form.value.email, form.value.password)
      .then(data => {
        loading.dismiss();
      })
      .catch(error => {
        loading.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Inicio de sesión falló!',
          message: error.message,
          buttons: ['OK']
        });
        alert.present();
      });
  }
}
