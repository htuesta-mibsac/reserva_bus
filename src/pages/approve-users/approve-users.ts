import { Component, OnInit } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { DataService } from '../../services/data';

@Component({
  selector: 'page-approve-users',
  templateUrl: 'approve-users.html',
})
export class ApproveUsersPage implements OnInit {
  private bookings: Array<any> = [];

  constructor(private loadingCtrl: LoadingController, private dataService: DataService) {
  }

  ngOnInit() {
    const loading = this.loadingCtrl.create({
      content: 'Obteniendo datos...'
    });
    loading.present();
    let $this = this;
    this.dataService.getBookingsToApprove().once('value', function(result) {
      if (result.val()) {
        console.log(result.val());
        $this.bookings = Object.keys(result.val()).map(key => result.val()[key]);
      }
      loading.dismiss();
    });
  }

  approveBooking() {
    console.log('approveBooking');
  }

  rejectBooking() {
    console.log('rejectBooking');
  }
}
