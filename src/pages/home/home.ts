import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { BusPage } from '../bus/bus';
import { PassengerPage } from '../passenger/passenger';
import { AuthService } from '../../services/auth';
import busData from '../../data/bus';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private buses: Array<object>;
  private selectedBus: any;
  private selectedDate: string;
  private logonUser: any;

  constructor(private navCtrl: NavController, private toastController: ToastController, private authService: AuthService) {
    this.buses = busData;
    this.logonUser = { id: 'echumpitaz@mibsac.com', name: 'Esteban Chumpitaz' };
  }

  showBusLayout() {
    if (this.selectedBus && this.selectedDate) {
      this.navCtrl.push(BusPage, { bus: this.selectedBus, date: this.selectedDate, user: this.logonUser });
    } else {
      const toast = this.toastController.create({
        message: 'Debe seleccionar fecha y bus',
        duration: 2500
      });
      toast.present();
    }
  }

  showBusPassengerList() {
    if (this.selectedBus && this.selectedDate) {
      this.navCtrl.push(PassengerPage, { bus: this.selectedBus, date: this.selectedDate });
    } else {
      const toast = this.toastController.create({
        message: 'Debe seleccionar fecha y bus',
        duration: 2500
      });
      toast.present();
    }
  }

  logout() {
    this.authService.logout();
  }
}
