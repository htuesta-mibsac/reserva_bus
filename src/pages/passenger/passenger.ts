import { Component } from '@angular/core';
import { NavParams, LoadingController } from 'ionic-angular';
import { DataService } from '../../services/data';

@Component({
  selector: 'page-passenger',
  templateUrl: 'passenger.html',
})
export class PassengerPage {
  private bus: any;
  private tripDate: string;
  private passengers: any = [];

  constructor(private navParams: NavParams, private dataService: DataService,
    private loadingCtrl: LoadingController) {
    const loading = this.loadingCtrl.create({
      content: 'Obteniendo datos...'
    });
    loading.present();

    this.bus = this.navParams.get('bus');
    this.tripDate = this.navParams.get('date');

    let $this = this;
    this.dataService.getPassengerList(this.bus.id, this.tripDate).once('value', function(result) {
      if (result.val()) {
        $this.passengers = Object.keys(result.val()).map(key => result.val()[key]).sort((a, b) => a.seat - b.seat);
      }
      loading.dismiss();
    });
  }
}
